<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    private RequestStack $requestStack;
    private Request $request;
    private CategoryRepository $categoryRepository;

    public function __construct(RequestStack $requestStack, CategoryRepository $categoryRepository)
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->categoryRepository = $categoryRepository;
    }

     /**
     * @Route("/search", name="search.index")
     */
    public function index(): Response
    {
        $search = $this->request->query->get('search');

        return $this->render('category/search.html.twig', [
			
		]);
    }
}